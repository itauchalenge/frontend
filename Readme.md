## aframe-react-example

React example index

![aframe-react-boilerplate](https://cloud.githubusercontent.com/assets/674727/24401414/286adeec-1369-11e7-8c41-22810d22d8d0.png)

### Requirements

nodejs
npm

### Installation

To get started:

```bash
npm install
```

To publish build statics:

```bash
npm run build
```

### Serverless installation


```bash
npm install -g serverless
serverless deploy
```

### This project going to create

- A S3 bucket in order to upload static react example, with public permissions, and S3 webstatics configuration
- A Cloudfront distribution pointing to react bucket example.

### Configure aws variables in order to run cloudformation stack

- Set Follow variables
    - AWS_ACCESS_KEY_ID
    - AWS_DEFAULT_REGION
    - AWS_SECRET_ACCESS_KEY
    
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html